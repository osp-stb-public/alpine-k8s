alpine-k8s
----------

Alpine Linux with Kubernetes and some other small tools: _jq, curl, envsubst_

Small image to use in CI-Pipelines.

Sample for Gitlab-CI
```
# Template für Kubernetes-Deployment
.deploy_k8s:
  only:
    refs:
      - master
      - prod
  stage: deploy
  image: registry.gitlab.com/osp-stb-public/alpine-k8s:3-1.18
  before_script:
    # kube_config wird per Gitlab-CI-Variable(Typ=File) bereitgestellt
    - export KUBECONFIG=$kube_config
    
```

```
deploy_k8s_infra:
  extends: .deploy_k8s
  needs: [] 
  only:
    changes:
      - operations/**/*
      - .gitlab-ci.yml
  script:
    # secret_env wird per Gitlab-CI-Variable(Typ=File) bereitgestellt
    - cp $secret_env operations/k8s-oci/base/secret.env
    - kubectl apply -k operations/k8s-oci/overlay/$CI_COMMIT_REF_NAME
```
