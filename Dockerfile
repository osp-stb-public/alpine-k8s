FROM alpine:3
ARG KUBECTL_VERSION=1.18.6

RUN apk add --update --no-cache curl jq gettext
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    mv kubectl /usr/bin/kubectl && \
    chmod +x /usr/bin/kubectl
